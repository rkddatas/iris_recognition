import numpy as np
import cv2
import os

def recflection_remove(img):
    ret, mask = cv2.threshold(img, 150, 255, cv2.THRESH_BINARY)
    kernel = np.ones((5, 5), np.uint8)                          
    dilation = cv2.dilate(mask, kernel, iterations=1)           
    dst = cv2.inpaint(img, dilation, 5, cv2.INPAINT_TELEA)      
    return dst

# def nothing(x):
#     pass

def processing(image_path,r):         
    #70, 160                         
    image = cv2.imread(image_path)
    image = cv2.resize(image, (640, 480), interpolation=cv2.INTER_LINEAR)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.medianBlur(image, 11)
    # gray = cv2.GaussianBlur(image,(5,5),0)
    
    ret, _ = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, 50, param1=ret, param2=20, minRadius=60,
                               maxRadius=100)

    circles = circles[0, :, :]  
    circles = np.int16(np.around(circles))
    
    center_circle_x = 0
    center_circle_y = 0
    radius = 0
    count = 0
    for i in circles[:]:
        # print(i[0], i[1])
        # radus = i[2]
        # print(i[2])
        if (i[0] > 200 and i[0] < 450) and (i[1] > 150 and i[1] < 350):
            center_circle_x += i[0]
            center_circle_y += i[1]
            radius += i[2]
            count+=1
    center_circle_x = int(center_circle_x / count)
    center_circle_y = int(center_circle_y / count)
    radius = int(radius / count)
    
    image = image[center_circle_y - radius - r:center_circle_y + radius + r, center_circle_x - radius -r:center_circle_x + radius + r]
    
    return image, radius


def daugman_normalizaiton(image, height, width, r_in, r_out):       
    thetas = np.arange(0, 2 * np.pi, 2 * np.pi / width)  
    r_out = r_in + r_out
    # Create empty flatten image
    flat = np.zeros((height,width, 3), np.uint8)
    circle_x = int(image.shape[0] / 2)
    circle_y = int(image.shape[1] / 2)

    for i in range(width):
        for j in range(height):
            theta = thetas[i]  # value of theta coordinate
            r_pro = j / height  # value of r coordinate(normalized)

            # get coordinate of boundaries
            Xi = circle_x + r_in * np.cos(theta)
            Yi = circle_y + r_in * np.sin(theta)
            Xo = circle_x + r_out * np.cos(theta)
            Yo = circle_y + r_out * np.sin(theta)

            # the matched cartesian coordinates for the polar coordinates
            Xc = (1 - r_pro) * Xi + r_pro * Xo
            Yc = (1 - r_pro) * Yi + r_pro * Yo

            color = image[int(Xc)][int(Yc)]  # color of the pixel

            flat[j][i] = color
    return flat  # liang

class Images(object):

    def __init__(self,path):            

        self.path = path
        self._r = 55
        self._height = 60
        self._width = 360

    def get_imagepath(self):
        path_images=[]
        file_images=[]
        for filename in os.listdir(self.path):
            file_images.append(filename)
            path_images.append(os.path.join(self.path,filename))
        return file_images,path_images


    @property
    def collect_images(self):           
        success_images = []
        success_label = []
        success_id = []
        i=0
        for subdir, dirs, files in os.walk(self.path):
            for file in files:
                Path = os.path.join(subdir, file)
                Label = subdir[-5:-2]
                if Path.endswith(".bmp"):
                    # print (Path)
                    image_path = Path
                    image_label = Label

                    processing(image_path,self._r)
                    image_roi, round = processing(image_path,self._r)
                    image_roi = recflection_remove(image_roi)
                    image_nor =  daugman_normalizaiton(image_roi,self._height, self._width, round, self._r)
                    image_nor = cv2.cvtColor(image_nor, cv2.COLOR_BGR2GRAY)
                    image_nor = cv2.equalizeHist(image_nor)
                    # plt.imshow(image_nor, cmap="gray")
                    # plt.show()
                    success_images.append(image_nor)
                    success_label.append(image_label)
                    
                    # process_log.processing_log(image_id,1)
                    save_image_path = "{0}.jpg".format(i)
                    success_id.append(save_image_path)
                    cv2.imwrite("..\\normalization\\"+save_image_path,image_nor)
                    print("Success loading:"+" " + "%s"%image_path)
                    i = i + 1

        return np.array(success_images,dtype=np.float32).reshape(-1,60,360,1),success_id,success_label
